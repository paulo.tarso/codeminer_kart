class TournamentsController < ApplicationController

  before_action :set_tournament, only: :show

  def index
    @tournaments = Tournament.includes(:races).all
  end

  def show
    @racers = Racer.from_tournament(params[:id])
    @points = @racers.sum('1 * (case when position = 1 then 5 when position = 2 then 3 when position = 3 then 2 else 0 end)')
  end

  def create
    @tournament = Tournament.new(tournament_params)

    if @tournament.save
      render :show, status: :created
    else
      render json: @racer.errors, status: :unprocessable_entity
    end
  end

  private

  def set_tournament
    @tournament = Tournament.find(params[:id])
  end

  def tournament_params
    params.require(:tournament)
      .permit(:name)
  end
end
