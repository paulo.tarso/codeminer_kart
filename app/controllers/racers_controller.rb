class RacersController < ApplicationController

  before_action :set_racer, only: [:update, :show, :destroy]

  def index
    @racers = Racer.all
  end

  def show
  end

  def create
    @racer = Racer.new(racer_params)

    if @racer.save
      render :show, status: :created
    else
      render json: @racer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @racer.update(racer_params)
      render :show
    else
      render json: @racer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @racer.destroy
    head :no_content
  end


  private

  def set_racer
    @racer = Racer.find(params[:id])
  end

  def racer_params
    params.require(:racer)
      .permit(:name, :born_at, :image_url)
  end
end
