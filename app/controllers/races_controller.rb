class RacesController < ApplicationController

  before_action :set_race, only: [:update, :show, :destroy]

  def index
    @races = Race.includes(:placements).all
  end

  def show
  end

  def create
    @race = Race.new(race_params)

    if @race.save
      render :show, status: :created
    else
      render json: @racer.errors, status: :unprocessable_entity
    end
  end

  def update
    if @race.update(race_params)
      render :show
    else
      render json: @racer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @race.destroy
    head :no_content
  end

  private

  def set_race
    @race = Race.find(params[:id])
  end

  def race_params
    params.require(:race)
      .permit(:tournament_id, :place, :date, placements_attributes: [:id, :racer_id, :position])
  end
end
