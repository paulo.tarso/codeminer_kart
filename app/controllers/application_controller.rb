class ApplicationController < ActionController::API

  rescue_from ActionController::RoutingError, ActiveRecord::RecordNotFound, with: :not_found

  def not_found
    render json: { error: Rack::Utils::HTTP_STATUS_CODES[404] }, status: 404
  end
end
