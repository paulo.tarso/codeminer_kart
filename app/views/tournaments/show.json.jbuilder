json.id @tournament.id
json.name @tournament.name

json.racers do
  json.array! @racers do |racer|
    json.id racer.id
    json.name racer.name
    json.born_at racer.born_at
    json.image_url racer.image_url
    json.points @points[racer.id]
  end
end

json.races do
  json.array! @tournament.races do |race|
    json.id race.id
    json.tournament_id race.tournament_id
    json.place race.place
    json.date race.date
    json.placements do
      json.array! race.placements do |placement|
        json.id placement.id
        json.racer_id placement.racer_id
        json.position placement.position
        json.race_id placement.race_id
      end
    end
  end
end

