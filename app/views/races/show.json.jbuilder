json.id @race.id
json.tournament_id @race.tournament_id
json.place @race.place
json.date @race.date
json.placements do
  json.array! @race.placements do |placement|
    json.id placement.id
    json.racer_id placement.racer_id
    json.position placement.position
    json.race_id placement.race_id
  end
end
