class Race < ApplicationRecord
  has_many :racers, through: :placements
  has_many :placements, dependent: :destroy

  belongs_to :tournament



  validates :place, presence: true
  validates :date, presence: true

  accepts_nested_attributes_for :placements, allow_destroy: true
end
