class Tournament < ApplicationRecord
  has_many :races

  validates :name, presence: true
end
