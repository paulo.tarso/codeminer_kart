class Placement < ApplicationRecord
  belongs_to :racer
  belongs_to :race

  validates :position, presence: true, numericality: { only_integer: true, greater_than: 0 }, uniqueness: { scope: :race_id }
  validates :racer_id , uniqueness: { scope: :race_id }

end
