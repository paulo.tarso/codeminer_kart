class Racer < ApplicationRecord

  has_many :races, through: :placements
  has_many :placements, dependent: :destroy

  validates :name, presence: true
  validates :born_at, presence: true
  validate :validate_age

  scope :from_tournament, -> (tournament_id) { joins(placements: :race).where(race: {tournament_id: tournament_id}).group(:id) }

  private

  def validate_age
    if born_at.present? && born_at > 18.years.ago
      errors.add(:born_at, 'Racer should be over 18 years old.')
    end
  end

end
