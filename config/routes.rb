Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  resources "racers"

  resources "races", except: :update

  resources "tournaments", only: [:show, :index, :create]
  # Defines the root path route ("/")
  # root "articles#index"
end
