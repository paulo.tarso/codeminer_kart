class CreatePlacements < ActiveRecord::Migration[7.0]
  def change
    create_table :placements do |t|
      t.references :racer, null: false, foreign_key: true
      t.references :race, null: false, foreign_key: true
      t.integer :position, null: false

      t.timestamps
    end
  end
end
