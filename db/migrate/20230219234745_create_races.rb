class CreateRaces < ActiveRecord::Migration[7.0]
  def change
    create_table :races do |t|
      t.references :tournament, null: false, foreign_key: true
      t.string :place, null: false
      t.date :date, null: false
      t.timestamps
    end
  end
end
