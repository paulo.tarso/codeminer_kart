class AddUniquenessIndex < ActiveRecord::Migration[7.0]
  def change
    add_index :placements, [:race_id, :position], unique: true
    add_index :placements, [:racer_id, :race_id], unique: true
  end
end
